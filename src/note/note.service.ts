import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { NoteDto, UpdateNoteDto } from './dto/note.dto';
import { Note } from './entities/note.entity';

@Injectable()
export class NoteService {
  constructor(
    @InjectRepository(Note)
    private noteRepository: Repository<Note>,
    private userService: UserService,
  ) {}

  create(note: NoteDto) {
    const noteEntity = new Note();
    noteEntity.title = note.title;
    noteEntity.description = note.description;
    noteEntity.save();
  }

  get() {
    return this.noteRepository.find();
  }

  getSingleNote(id: number) {
    const note = this.noteRepository.findOne({
      where: { id: id },
    });
    return note;
  }

  async updateNote(note: UpdateNoteDto) {
    const n = await this.noteRepository.findOne({
      where: { id: note.id },
    });
    if (!n) {
      throw new NotFoundException('Note not found');
    }

    n.title = note.title;
    n.description = note.description;
    n.save();
  }

  deleteNote(id: number) {
    return this.noteRepository.softDelete(id);
  }
}

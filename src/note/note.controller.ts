import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { NoteDto, UpdateNoteDto } from './dto/note.dto';
import { NoteService } from './note.service';

@Controller('note')
export class NoteController {
  constructor(private noteService: NoteService) {}

  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }))
  createNote(@Body() body: NoteDto) {
    return this.noteService.create(body);
  }

  @Get()
  getNote() {
    return this.noteService.get();
  }

  @Get(':id')
  getSingleNote(@Param('id') id: number) {
    return this.noteService.getSingleNote(+id);
  }

  @Put()
  @UsePipes(new ValidationPipe())
  async updateNote(@Body() body: UpdateNoteDto) {
    return await this.noteService.updateNote(body);
  }

  @Delete(':id')
  deleteNote(@Param('id') id: number) {
    return this.noteService.deleteNote(id);
  }
}

import { Controller, Delete, Get, Post, Put } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post()
  create() {
    return 'post method';
  }

  @Put()
  update() {
    return 'put method';
  }

  @Delete()
  delete() {
    return 'delete method';
  }
}
